
*------------------------------------
* Hernán MA, Robins JM (2020). 
*   Causal Inference: What If. 
*   Boca Raton: Chapman & Hall/CRC.
*------------------------------------
* Book Web: https://www.hsph.harvard.edu/miguel-hernan/causal-inference-book/

* Author (dofile): Eleanor Murray 
* For errors contact: ejmurray@bu.edu
* 
* Update: Yujun Lian (arlionn@163.com)
*         https://www.lianxh.cn
* 添加了变量标签，修改了 dofile 名称

* 项目主页：https://gitee.com/arlionn/CI


global path "D:\Lec\Robin_CI2020" //酌情修改路径

cd $path 

doedit "chapter11.do"  //打开第11章的dofile

doedit "chapter12.do"

doedit "chapter13.do"

doedit "chapter14.do"

doedit "chapter15.do"

doedit "chapter16.do"

doedit "chapter17.do"













*------------------
*- 前期数据处理    - 无用了
*------------------


use "nhefs00.dta", clear 
*label data "Robins_2020_Cause_Inference_What_IF datasets, original, 2020/2/28"
*save nhefs00.dta, replace 

do var_label.do

save "nhefs.dta", replace 



